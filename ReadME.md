# ServiceJob Spring Boot

This project is a RESTful API for job posting and job application. It is built with Spring Boot framework and uses MySQL as the database.

## API Endpoints

The following are the available endpoints or you can import from `docs/Job Posting Rest Api.postman_collection.json` into Postman:

### Login

This endpoint allows users to login.

- URL: `/user/login`
- Method: `POST`
- Content-Type: `multipart/form-data`
- Body:
  - `email`: Email candidate `candidate@email.com` or Email company `company@email.com`
  - `password`: `halo123@#`
- Response:
  - HTTP Status Code: `200`
  - Body: 
  ```json
  {
    "code": 200,
    "success": "Authorized",
    "message": "Success Login",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjYW5kaWRhdGVAZW1haWwuY29tIiwicm9sZSI6IkNhbmRpZGF0ZSIsImlkZW50aWZ5IjoiMDowOjA6MDowOjA6MDoxIiwiaXNfZW5hYmxlIjp0cnVlLCJpc3MiOiJkZXZlbG9wZXIiLCJleHAiOjE2ODQwODA5NDR9.dUhgl_UaaqzSp953sZzQIm1duvIek1SlfSbKOmrm_dGEsXD1rWxUvCKNFIO81S8gfHAca8c6fioWW_X4vDat4w"
  }
  ```

### Fetch All Job Postings

This endpoint allows users to retrieve all job postings that match certain criteria, such as search keywords and job category.

- URL: `/jobPostings`
- Method: `GET`
- Query Parameters:
    - `search` (optional): search keywords for job title and description
    - `category` (optional): job category
- Headers:
    - `Authorization`: Bearer token for authentication
- Response:
    - HTTP Status Code: `200`
    - Body: array of job posting objects

### Apply Job

This endpoint allows users to apply for a job posting by submitting a cover letter and resume.

- URL: `/jobPostings/apply-job`
- Method: `POST`
- Query Parameters:
    - `idJob`: the ID of the job posting to apply for
- Headers:
    - `Authorization`: Bearer token for authentication
    - `X-USER-EMAIL`: email of the applicant (required but already handle by `CustomAuthorizationFilter.class`)
- Content-Type: `multipart/form-data`
- Body:
    - `cover_letter`: text
    - `resume`: file
- Response:
    - HTTP Status Code: `201`
    - Body: success message

### List Applicant

This endpoint allows job posters to retrieve a list of applicants who have applied for their job posting.

- URL: `/jobPostings/applicant`
- Method: `GET`
- Query Parameters:
    - `idJob`: the ID of the job posting to retrieve applicants for
- Headers:
    - `Authorization`: Bearer token for authentication
- Response:
    - HTTP Status Code: `200`
    - Body: array of applicant objects

## Dependencies

This project uses the following dependencies:

- Spring Web: for creating RESTful APIs
- Spring Data JPA: for working with relational databases
- MySQL Driver: for connecting to MySQL database
- Spring Security: for authentication and authorization
- Auth0 JWT: for JSON Web Token (JWT) authentication
- Flywaydb: for migrate Database
- Amazon Aws SDK: for space bucket resume upload

## Configuration

To run this project, the following configuration is required:

- MySQL database
- Java Development Kit (JDK) 17 or later

The following environment variables should be set:

- `DATABASE_URL`: URL to connect to MySQL database
- `DATABASE_USERNAME`: MySQL username
- `DATABASE_PASSWORD`: MySQL password
- `SPACE_FOLDER`: folder saved resume
- `SPACE_KEY`: Space Digital Ocean API Key
- `SPACE_SECRET`: Space Digital Ocean Secret Key
- `SPACE_ENDPOINT`: Space Digital Ocean Endpoint
- `SPACE_REGION`: Space Digital Ocean Region
- `SPACE_BUCKET`: Space Digital Ocean Bucket
- `JWT_SECRET`: secret key for generating and validating JWT tokens
- `JWT_EXPIRATION_MS`: expiration time for JWT tokens (in milliseconds)
- `JWT_ISSUER`: for set issuer JWT tokens

## How to Run

To run this project, follow these steps:

1. Clone this repository
2. Set the required environment variables
3. Navigate to the project directory
4. Run the following command: `./mvnw spring-boot:run`

## Contributors

- [M Lukman](https://gitlab.com/iNitialM503) - creator and maintainer