package dev.lukman.servicejob.model;

import dev.lukman.servicejob.dto.JobPostingDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "job_posting")
@Accessors(chain = true)
public class JobPosting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String category;

    @Column(nullable = false)
    private String location;

    @Column(name = "company_name", nullable = false)
    private String companyName;

    @Column(name = "date_posted", nullable = false)
    private Date datePosted;

    @Column(name = "date_expired", nullable = false)
    private Date dateExpired;

    @OneToMany(mappedBy = "jobPosting", cascade = CascadeType.ALL)
    private List<JobApplication> applications = new ArrayList<>();

    public JobPostingDTO fetchAll(){
        return new JobPostingDTO()
                .setId(id)
                .setCategory(category)
                .setTitle(title)
                .setDescription(description)
                .setLocation(location)
                .setCompanyName(companyName)
                .setDatePosted(datePosted)
                .setDateExpired(dateExpired);
    }
}