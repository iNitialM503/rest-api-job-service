package dev.lukman.servicejob.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface JobPostingServices {

    ResponseEntity<Object> fetchAll(String search, String category) throws Exception;

    ResponseEntity<Object> applyJob(Long id, MultipartFile resume, String coverLetter, String email) throws Exception;

    ResponseEntity<Object> getCandidate(Long id) throws Exception;
}
