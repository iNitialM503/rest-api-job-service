package dev.lukman.servicejob.service;

import dev.lukman.servicejob.dto.CredentialDTO;
import dev.lukman.servicejob.model.JobPosting;


public interface JobApplicationServices {

    void submitJob(JobPosting jobPosting, String resumeUrl, String coverLetter, CredentialDTO creds) throws Exception;
}
