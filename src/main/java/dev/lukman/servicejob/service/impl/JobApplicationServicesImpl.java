package dev.lukman.servicejob.service.impl;

import dev.lukman.servicejob.dto.CredentialDTO;
import dev.lukman.servicejob.model.JobApplication;
import dev.lukman.servicejob.model.JobPosting;
import dev.lukman.servicejob.repository.JobApplicationRepository;
import dev.lukman.servicejob.service.JobApplicationServices;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class JobApplicationServicesImpl implements JobApplicationServices {

    private final JobApplicationRepository jobApplicationRepository;

    @Override
    public void submitJob(JobPosting jobPosting, String resumeUrl, String coverLetter, CredentialDTO creds) throws Exception {
        try{
            JobApplication data = creds.toJobApplication(jobPosting,resumeUrl,coverLetter);
            jobApplicationRepository.save(data);
        } catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
}
