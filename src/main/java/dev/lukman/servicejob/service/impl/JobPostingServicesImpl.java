package dev.lukman.servicejob.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import dev.lukman.servicejob.api.ResponseJobApplication;
import dev.lukman.servicejob.api.ResponseJobPosting;
import dev.lukman.servicejob.api.SuccessResponse;
import dev.lukman.servicejob.dto.JobApplicationDTO;
import dev.lukman.servicejob.dto.JobPostingDTO;
import dev.lukman.servicejob.exception.BadRequestException;
import dev.lukman.servicejob.exception.ResourceNotFoundException;
import dev.lukman.servicejob.model.JobApplication;
import dev.lukman.servicejob.model.JobPosting;
import dev.lukman.servicejob.model.User;
import dev.lukman.servicejob.repository.JobPostingRepository;
import dev.lukman.servicejob.repository.UserRepository;
import dev.lukman.servicejob.service.JobApplicationServices;
import dev.lukman.servicejob.service.JobPostingServices;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class JobPostingServicesImpl implements JobPostingServices {

    private final UserRepository userRepository;
    private final JobPostingRepository jobPostingRepository;

    private final JobApplicationServices jobApplicationServices;
    private final AmazonS3 s3Client;
    @Value("${do.space.bucket}")
    private String doSpaceBucket;
    @Value("${do.space.folder}")
    private String folderName;
    @Override
    public ResponseEntity<Object> fetchAll(String search, String category) throws Exception {
        try{
            List<JobPosting> jobPostings;
            if (search != null && category != null){
                jobPostings = jobPostingRepository.findByKeywordAndCategory(search, category);
            } else if (search != null){
                jobPostings = jobPostingRepository.findByKeyword(search);
            } else if (category != null){
                jobPostings = jobPostingRepository.findAllByCategory(category);
            } else {
                jobPostings = jobPostingRepository.findAll();
            }
            List<JobPostingDTO> data = jobPostings.stream()
                    .map(JobPosting::fetchAll)
                    .collect(Collectors.toList());

            return ResponseEntity.status(HttpStatus.OK)
                    .body(ResponseJobPosting.success(data));
        } catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
    @Override
    public ResponseEntity<Object> applyJob(Long id, MultipartFile resume, String coverLetter, String email) throws Exception {
        try {
            JobPosting jobPosting = jobPostingRepository.findById(id)
                    .filter(jp -> jp.getDateExpired().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isAfter(LocalDate.now()) || jp.getDateExpired().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual(LocalDate.now()))
                    .orElseThrow(() -> new ResourceNotFoundException("Job Posting not found or expired"));
            String idJob = jobPosting.getId() + "/";
            String extension = FilenameUtils.getExtension(resume.getOriginalFilename());
            String fileName = FilenameUtils.removeExtension(resume.getOriginalFilename());
            String key = folderName + idJob + fileName + "." + extension;
            String resumeUrl = saveResumeToSpace(resume, key);
            Optional<User> user = userRepository.findByEmail(email);
            if (user.isPresent()){
                jobApplicationServices.submitJob(jobPosting, resumeUrl, coverLetter, user.get().sentToCredentialDTO());
            }else {
                throw new BadRequestException("Bad Request");
            }

            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(SuccessResponse.success("Success", HttpStatus.CREATED.value(), "Job Application has submitted"));
        } catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    private String saveResumeToSpace(MultipartFile multipartFile, String key) throws IOException {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(multipartFile.getInputStream().available());
        if (multipartFile.getContentType() != null && !"".equals(multipartFile.getContentType())) {
            metadata.setContentType(multipartFile.getContentType());
        }
        s3Client.putObject(new PutObjectRequest(doSpaceBucket, key, multipartFile.getInputStream(), metadata)
                .withCannedAcl(CannedAccessControlList.PublicRead));
        return s3Client.getUrl(doSpaceBucket, key).toString();
    }

    @Override
    public ResponseEntity<Object> getCandidate(Long id) throws Exception {
        try{
            Optional<JobPosting> optionalJobPosting = jobPostingRepository.findById(id);
            if (optionalJobPosting.isEmpty()) {
                return new ResponseEntity<>("JobPosting with id " + id + " not found.", HttpStatus.NOT_FOUND);
            }

            JobPosting jobPosting = optionalJobPosting.get();
            List<JobApplication> applications = jobPosting.getApplications();
            List<JobApplicationDTO> applicationDTOs = new ArrayList<>();

            for (JobApplication application : applications) {
                applicationDTOs.add(new JobApplicationDTO(application));
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .body(ResponseJobApplication.success(applicationDTOs));
        } catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

}
