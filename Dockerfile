FROM --platform=linux/amd64 maven:3-eclipse-temurin-17-alpine as builder
WORKDIR /service-job
ADD . .
RUN mvn -f pom.xml clean package

FROM --platform=linux/amd64 openjdk:17-alpine
WORKDIR /service-job
COPY --from=builder /service-job/target/*.jar /service-job/cloud-gateway.jar
RUN apk add --no-cache iputils
RUN chmod +x /service-job/service-job.jar
EXPOSE 9191
ENTRYPOINT ["java", "-jar", "service-job.jar"]